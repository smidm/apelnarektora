from django import template
from django.utils.safestring import mark_safe

from fitauth import get_roles, get_faculties

register = template.Library()


@register.filter(is_safe=True)
def detail(user):
    if user.get_full_name():
        output = '%s (%s)' % (user.get_full_name(), user.username)
    else:
        output = user.username

    roles = get_roles(user)
    if 'B-00000-ZAMESTNANEC-AKADEMICKY' in roles:
        output += ' <span class="label label-info">zaměstnanec</span>'
    if 'B-00000-STUDENT' in roles:
        output += ' <span class="label label-primary">student</span>'

    return mark_safe(output)


@register.filter(is_safe=True)
def anonymous_detail(user):
    roles = get_roles(user)

    output = "(jméno skryto)"

    if 'B-00000-ZAMESTNANEC-AKADEMICKY' in roles:
        output += ' <span class="label label-info">zaměstnanec</span>'
    if 'B-00000-STUDENT' in roles:
        output += ' <span class="label label-primary">student</span>'

    return mark_safe(output)


@register.filter(is_safe=True)
def faculties(user):
    return mark_safe(" ".join(['<span class="label label-warning">{}</span>'.format(x) for x in get_faculties(user)]))
